package com.drchase.hiraganakatakanafacelet;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.chase.hiragana.RomajiLettersHir;

@RequestScoped
@Named
public class RomajiToHiraganaConverter {
	private String romajiInput;
	private String hiraganaOutput;
	
	public String getRomajiInput() {
		return romajiInput;
	}
	public void setRomajiInput(String romajiInput) {
		this.romajiInput = romajiInput;
	}
	public String getHiraganaOutput() {
		return hiraganaOutput;
	}
	public void setHiraganaOutput(String hiraganaOutput) {
		this.hiraganaOutput = hiraganaOutput;
	}
	
	public void convertRomajiToHiragana()	{
		RomajiLettersHir converter = RomajiLettersHir.getInstance();
		
		hiraganaOutput = converter.convertToHiragana(romajiInput);
	}
	
}
