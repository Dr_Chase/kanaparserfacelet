package com.drchase.hiraganakatakanafacelet;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.chase.katakana.KatakanaLetters;

@RequestScoped
@Named
public class KatakanaAToRomajiConverter {
    private String katakanaInput = null;
    private String romajiOutput = null;

    public String getKatakanaInput() {
        return katakanaInput;
    }

    public void setKatakanaInput(String katakanaInput) {
        this.katakanaInput = katakanaInput;
    }

    public String getRomajiOutput() {
        return romajiOutput;
    }

    public void setRomajiOutput(String romajiOutput) {
        this.romajiOutput = romajiOutput;
    }

    public void convertKatakanaToRomaji()   {
    	KatakanaLetters hiraganaParser =  KatakanaLetters.getInstance();
        romajiOutput = hiraganaParser.parseKatakanaString(katakanaInput);
    }

}
