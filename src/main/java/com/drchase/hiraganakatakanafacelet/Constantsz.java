package com.drchase.hiraganakatakanafacelet;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

@ApplicationScoped
@Named
@ManagedBean(name="constantsBean")

public class Constantsz {
	
	/* we need to create getters and setters otherwise this doesn't work */
	
	private String  rowNum = new Integer(3).toString() ;
	private String colNum = new Integer(24).toString() ;
	
	public String getRowNum() {
		return rowNum;
	}
	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}
	public String getColNum() {
		return colNum;
	}
	public void setColNum(String colNum) {
		this.colNum = colNum;
	}
	
	
}
