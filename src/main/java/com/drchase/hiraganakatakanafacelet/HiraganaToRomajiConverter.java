/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.drchase.hiraganakatakanafacelet;


import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.chase.katakana.RomajiLettersKat;
import com.chase.katakana.KatakanaLetters;
import com.chase.hiragana.RomajiLettersHir;
import com.chase.hiragana.HiraganaLetters;
/**
 *
 * @author Dr.Chase
 */
@RequestScoped
@Named
public class HiraganaToRomajiConverter {
    private String hiraganaInput = null;
    private String romajiOutput = null;

    public String getHiraganaInput() {
        return hiraganaInput;
    }

    public void setHiraganaInput(String hiraganaInput) {
        this.hiraganaInput = hiraganaInput;
    }

    public String getRomajiOutput() {
        return romajiOutput;
    }

    public void setRomajiOutput(String romajiOutput) {
        this.romajiOutput = romajiOutput;
    }

    public void convertHiraganaToRomaji()   {
    	HiraganaLetters hiraganaParser = HiraganaLetters.getInstance();
        romajiOutput = hiraganaParser.parseHiraganaString(hiraganaInput);
    }
}
