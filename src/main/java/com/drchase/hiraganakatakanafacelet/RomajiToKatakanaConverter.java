package com.drchase.hiraganakatakanafacelet;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.chase.katakana.RomajiLettersKat;

@RequestScoped
@Named
public class RomajiToKatakanaConverter {
	
	private String romajiInput;
	private String katakanaOutput;

	public String getRomajiInput() {
		return romajiInput;
	}

	public void setRomajiInput(String romajiInput) {
		this.romajiInput = romajiInput;
	}

	public String getKatakanaOutput() {
		return katakanaOutput;
	}

	public void setKatakanaOutput(String katakanaOutput) {
		this.katakanaOutput = katakanaOutput;
	}
	
	public void convertRomajiToKatakana()	{
		RomajiLettersKat converter = RomajiLettersKat.getInstance();
		
		katakanaOutput = converter.parseKatakanaString(romajiInput);
	}


}
